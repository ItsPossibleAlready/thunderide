/* About.c generated by valac 0.36.15, the Vala compiler
 * generated from About.vala, do not modify */


#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <stdlib.h>
#include <string.h>


#define THUNDER_TYPE_ABOUT_WINDOW (thunder_about_window_get_type ())
#define THUNDER_ABOUT_WINDOW(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), THUNDER_TYPE_ABOUT_WINDOW, ThunderAboutWindow))
#define THUNDER_ABOUT_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), THUNDER_TYPE_ABOUT_WINDOW, ThunderAboutWindowClass))
#define THUNDER_IS_ABOUT_WINDOW(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), THUNDER_TYPE_ABOUT_WINDOW))
#define THUNDER_IS_ABOUT_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), THUNDER_TYPE_ABOUT_WINDOW))
#define THUNDER_ABOUT_WINDOW_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), THUNDER_TYPE_ABOUT_WINDOW, ThunderAboutWindowClass))

typedef struct _ThunderAboutWindow ThunderAboutWindow;
typedef struct _ThunderAboutWindowClass ThunderAboutWindowClass;
typedef struct _ThunderAboutWindowPrivate ThunderAboutWindowPrivate;

struct _ThunderAboutWindow {
	GtkAboutDialog parent_instance;
	ThunderAboutWindowPrivate * priv;
};

struct _ThunderAboutWindowClass {
	GtkAboutDialogClass parent_class;
};


static gpointer thunder_about_window_parent_class = NULL;

GType thunder_about_window_get_type (void) G_GNUC_CONST;
enum  {
	THUNDER_ABOUT_WINDOW_0_PROPERTY
};
ThunderAboutWindow* thunder_about_window_new (void);
ThunderAboutWindow* thunder_about_window_construct (GType object_type);
static void __lambda13_ (ThunderAboutWindow* self, gint response_id);
static void ___lambda13__gtk_dialog_response (GtkDialog* _sender, gint response_id, gpointer self);
static void _vala_array_destroy (gpointer array, gint array_length, GDestroyNotify destroy_func);
static void _vala_array_free (gpointer array, gint array_length, GDestroyNotify destroy_func);


static void __lambda13_ (ThunderAboutWindow* self, gint response_id) {
	gboolean _tmp0_ = FALSE;
	gint _tmp1_;
#line 14 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	_tmp1_ = response_id;
#line 14 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	if (_tmp1_ == ((gint) GTK_RESPONSE_CANCEL)) {
#line 14 "/home/user/Projects/thunderide/src/Widgets/About.vala"
		_tmp0_ = TRUE;
#line 57 "About.c"
	} else {
		gint _tmp2_;
#line 14 "/home/user/Projects/thunderide/src/Widgets/About.vala"
		_tmp2_ = response_id;
#line 14 "/home/user/Projects/thunderide/src/Widgets/About.vala"
		_tmp0_ = _tmp2_ == ((gint) GTK_RESPONSE_DELETE_EVENT);
#line 64 "About.c"
	}
#line 14 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	if (_tmp0_) {
#line 15 "/home/user/Projects/thunderide/src/Widgets/About.vala"
		gtk_widget_hide_on_delete ((GtkWidget*) self);
#line 70 "About.c"
	}
}


static void ___lambda13__gtk_dialog_response (GtkDialog* _sender, gint response_id, gpointer self) {
#line 13 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	__lambda13_ ((ThunderAboutWindow*) self, response_id);
#line 78 "About.c"
}


ThunderAboutWindow* thunder_about_window_construct (GType object_type) {
	ThunderAboutWindow * self = NULL;
	gchar* _tmp0_;
	gchar** _tmp1_;
	gchar** _tmp2_;
	gint _tmp2__length1;
	gchar* _tmp3_;
	gchar** _tmp4_;
	gchar** _tmp5_;
	gint _tmp5__length1;
#line 3 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	self = (ThunderAboutWindow*) g_object_new (object_type, NULL);
#line 4 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	gtk_window_set_destroy_with_parent ((GtkWindow*) self, TRUE);
#line 6 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	_tmp0_ = g_strdup ("Nurdaulet Kurenshe");
#line 6 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	_tmp1_ = g_new0 (gchar*, 1 + 1);
#line 6 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	_tmp1_[0] = _tmp0_;
#line 6 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	_tmp2_ = _tmp1_;
#line 6 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	_tmp2__length1 = 1;
#line 6 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	gtk_about_dialog_set_artists ((GtkAboutDialog*) self, _tmp2_);
#line 6 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	_tmp2_ = (_vala_array_free (_tmp2_, _tmp2__length1, (GDestroyNotify) g_free), NULL);
#line 7 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	_tmp3_ = g_strdup ("Nurdaulet Kurenshe");
#line 7 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	_tmp4_ = g_new0 (gchar*, 1 + 1);
#line 7 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	_tmp4_[0] = _tmp3_;
#line 7 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	_tmp5_ = _tmp4_;
#line 7 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	_tmp5__length1 = 1;
#line 7 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	gtk_about_dialog_set_authors ((GtkAboutDialog*) self, _tmp5_);
#line 7 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	_tmp5_ = (_vala_array_free (_tmp5_, _tmp5__length1, (GDestroyNotify) g_free), NULL);
#line 9 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	gtk_about_dialog_set_program_name ((GtkAboutDialog*) self, "ThunderIDE");
#line 10 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	gtk_about_dialog_set_comments ((GtkAboutDialog*) self, "ThunderIDE is a GTK based IDE for Vala programming");
#line 11 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	gtk_about_dialog_set_version ((GtkAboutDialog*) self, "Beta1");
#line 13 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	g_signal_connect_object ((GtkDialog*) self, "response", (GCallback) ___lambda13__gtk_dialog_response, self, 0);
#line 18 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	gtk_window_present ((GtkWindow*) self);
#line 3 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	return self;
#line 136 "About.c"
}


ThunderAboutWindow* thunder_about_window_new (void) {
#line 3 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	return thunder_about_window_construct (THUNDER_TYPE_ABOUT_WINDOW);
#line 143 "About.c"
}


static void thunder_about_window_class_init (ThunderAboutWindowClass * klass) {
#line 2 "/home/user/Projects/thunderide/src/Widgets/About.vala"
	thunder_about_window_parent_class = g_type_class_peek_parent (klass);
#line 150 "About.c"
}


static void thunder_about_window_instance_init (ThunderAboutWindow * self) {
}


GType thunder_about_window_get_type (void) {
	static volatile gsize thunder_about_window_type_id__volatile = 0;
	if (g_once_init_enter (&thunder_about_window_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (ThunderAboutWindowClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) thunder_about_window_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (ThunderAboutWindow), 0, (GInstanceInitFunc) thunder_about_window_instance_init, NULL };
		GType thunder_about_window_type_id;
		thunder_about_window_type_id = g_type_register_static (gtk_about_dialog_get_type (), "ThunderAboutWindow", &g_define_type_info, 0);
		g_once_init_leave (&thunder_about_window_type_id__volatile, thunder_about_window_type_id);
	}
	return thunder_about_window_type_id__volatile;
}


static void _vala_array_destroy (gpointer array, gint array_length, GDestroyNotify destroy_func) {
	if ((array != NULL) && (destroy_func != NULL)) {
		int i;
		for (i = 0; i < array_length; i = i + 1) {
			if (((gpointer*) array)[i] != NULL) {
				destroy_func (((gpointer*) array)[i]);
			}
		}
	}
}


static void _vala_array_free (gpointer array, gint array_length, GDestroyNotify destroy_func) {
	_vala_array_destroy (array, array_length, destroy_func);
	g_free (array);
}



