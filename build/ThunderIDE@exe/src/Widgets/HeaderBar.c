/* HeaderBar.c generated by valac 0.36.15, the Vala compiler
 * generated from HeaderBar.vala, do not modify */


#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>


#define THUNDER_TYPE_HEADER_BAR (thunder_header_bar_get_type ())
#define THUNDER_HEADER_BAR(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), THUNDER_TYPE_HEADER_BAR, ThunderHeaderBar))
#define THUNDER_HEADER_BAR_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), THUNDER_TYPE_HEADER_BAR, ThunderHeaderBarClass))
#define THUNDER_IS_HEADER_BAR(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), THUNDER_TYPE_HEADER_BAR))
#define THUNDER_IS_HEADER_BAR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), THUNDER_TYPE_HEADER_BAR))
#define THUNDER_HEADER_BAR_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), THUNDER_TYPE_HEADER_BAR, ThunderHeaderBarClass))

typedef struct _ThunderHeaderBar ThunderHeaderBar;
typedef struct _ThunderHeaderBarClass ThunderHeaderBarClass;
typedef struct _ThunderHeaderBarPrivate ThunderHeaderBarPrivate;

#define THUNDER_TYPE_EDIT_NOTEBOOK (thunder_edit_notebook_get_type ())
#define THUNDER_EDIT_NOTEBOOK(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), THUNDER_TYPE_EDIT_NOTEBOOK, ThunderEditNotebook))
#define THUNDER_EDIT_NOTEBOOK_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), THUNDER_TYPE_EDIT_NOTEBOOK, ThunderEditNotebookClass))
#define THUNDER_IS_EDIT_NOTEBOOK(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), THUNDER_TYPE_EDIT_NOTEBOOK))
#define THUNDER_IS_EDIT_NOTEBOOK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), THUNDER_TYPE_EDIT_NOTEBOOK))
#define THUNDER_EDIT_NOTEBOOK_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), THUNDER_TYPE_EDIT_NOTEBOOK, ThunderEditNotebookClass))

typedef struct _ThunderEditNotebook ThunderEditNotebook;
typedef struct _ThunderEditNotebookClass ThunderEditNotebookClass;

#define THUNDER_TYPE_DOCUMENT (thunder_document_get_type ())
#define THUNDER_DOCUMENT(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), THUNDER_TYPE_DOCUMENT, ThunderDocument))
#define THUNDER_DOCUMENT_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), THUNDER_TYPE_DOCUMENT, ThunderDocumentClass))
#define THUNDER_IS_DOCUMENT(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), THUNDER_TYPE_DOCUMENT))
#define THUNDER_IS_DOCUMENT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), THUNDER_TYPE_DOCUMENT))
#define THUNDER_DOCUMENT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), THUNDER_TYPE_DOCUMENT, ThunderDocumentClass))

typedef struct _ThunderDocument ThunderDocument;
typedef struct _ThunderDocumentClass ThunderDocumentClass;

#define THUNDER_TYPE_MAIN_WINDOW (thunder_main_window_get_type ())
#define THUNDER_MAIN_WINDOW(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), THUNDER_TYPE_MAIN_WINDOW, ThunderMainWindow))
#define THUNDER_MAIN_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), THUNDER_TYPE_MAIN_WINDOW, ThunderMainWindowClass))
#define THUNDER_IS_MAIN_WINDOW(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), THUNDER_TYPE_MAIN_WINDOW))
#define THUNDER_IS_MAIN_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), THUNDER_TYPE_MAIN_WINDOW))
#define THUNDER_MAIN_WINDOW_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), THUNDER_TYPE_MAIN_WINDOW, ThunderMainWindowClass))

typedef struct _ThunderMainWindow ThunderMainWindow;
typedef struct _ThunderMainWindowClass ThunderMainWindowClass;
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))

struct _ThunderHeaderBar {
	GtkHeaderBar parent_instance;
	ThunderHeaderBarPrivate * priv;
};

struct _ThunderHeaderBarClass {
	GtkHeaderBarClass parent_class;
};


static gpointer thunder_header_bar_parent_class = NULL;
extern ThunderEditNotebook* thunder_notebook;
extern ThunderMainWindow* thunder_window;

GType thunder_header_bar_get_type (void) G_GNUC_CONST;
enum  {
	THUNDER_HEADER_BAR_0_PROPERTY
};
ThunderHeaderBar* thunder_header_bar_new (void);
ThunderHeaderBar* thunder_header_bar_construct (GType object_type);
void thunder_header_bar_header_bar_check (ThunderHeaderBar* self);
static void __lambda4_ (ThunderHeaderBar* self);
GType thunder_edit_notebook_get_type (void) G_GNUC_CONST;
void thunder_edit_notebook_new_file (ThunderEditNotebook* self);
static void ___lambda4__gtk_button_clicked (GtkButton* _sender, gpointer self);
static void __lambda10_ (ThunderHeaderBar* self);
void thunder_OpenDialogChooser (void);
static void ___lambda10__gtk_button_clicked (GtkButton* _sender, gpointer self);
static void __lambda11_ (ThunderHeaderBar* self);
GType thunder_document_get_type (void) G_GNUC_CONST;
ThunderDocument* thunder_edit_notebook_get_current (ThunderEditNotebook* self);
void thunder_document_save_as (ThunderDocument* self);
GType thunder_main_window_get_type (void) G_GNUC_CONST;
static void ___lambda11__gtk_button_clicked (GtkButton* _sender, gpointer self);
static void __lambda12_ (ThunderHeaderBar* self);
void thunder_document_save (ThunderDocument* self);
static void ___lambda12__gtk_button_clicked (GtkButton* _sender, gpointer self);


ThunderHeaderBar* thunder_header_bar_construct (GType object_type) {
	ThunderHeaderBar * self = NULL;
#line 3 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	self = (ThunderHeaderBar*) g_object_new (object_type, NULL);
#line 4 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	gtk_header_bar_set_title ((GtkHeaderBar*) self, "ThunderIDE");
#line 5 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	gtk_header_bar_set_subtitle ((GtkHeaderBar*) self, "Write Code Like Thunder");
#line 6 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	gtk_header_bar_set_show_close_button ((GtkHeaderBar*) self, TRUE);
#line 7 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	thunder_header_bar_header_bar_check (self);
#line 3 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	return self;
#line 106 "HeaderBar.c"
}


ThunderHeaderBar* thunder_header_bar_new (void) {
#line 3 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	return thunder_header_bar_construct (THUNDER_TYPE_HEADER_BAR);
#line 113 "HeaderBar.c"
}


static void __lambda4_ (ThunderHeaderBar* self) {
	ThunderEditNotebook* _tmp0_;
#line 12 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	_tmp0_ = thunder_notebook;
#line 12 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	thunder_edit_notebook_new_file (_tmp0_);
#line 123 "HeaderBar.c"
}


static void ___lambda4__gtk_button_clicked (GtkButton* _sender, gpointer self) {
#line 11 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	__lambda4_ ((ThunderHeaderBar*) self);
#line 130 "HeaderBar.c"
}


static void __lambda10_ (ThunderHeaderBar* self) {
	ThunderEditNotebook* _tmp0_;
#line 19 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	thunder_OpenDialogChooser ();
#line 20 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	_tmp0_ = thunder_notebook;
#line 20 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	gtk_widget_show_all ((GtkWidget*) _tmp0_);
#line 142 "HeaderBar.c"
}


static void ___lambda10__gtk_button_clicked (GtkButton* _sender, gpointer self) {
#line 18 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	__lambda10_ ((ThunderHeaderBar*) self);
#line 149 "HeaderBar.c"
}


static void __lambda11_ (ThunderHeaderBar* self) {
	ThunderEditNotebook* _tmp0_;
	ThunderDocument* _tmp1_;
	ThunderDocument* _tmp2_;
	ThunderMainWindow* _tmp3_;
#line 26 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	_tmp0_ = thunder_notebook;
#line 26 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	_tmp1_ = thunder_edit_notebook_get_current (_tmp0_);
#line 26 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	_tmp2_ = _tmp1_;
#line 26 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	thunder_document_save_as (_tmp2_);
#line 27 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	_tmp3_ = thunder_window;
#line 27 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	gtk_widget_show_all ((GtkWidget*) _tmp3_);
#line 170 "HeaderBar.c"
}


static void ___lambda11__gtk_button_clicked (GtkButton* _sender, gpointer self) {
#line 25 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	__lambda11_ ((ThunderHeaderBar*) self);
#line 177 "HeaderBar.c"
}


static void __lambda12_ (ThunderHeaderBar* self) {
	ThunderEditNotebook* _tmp0_;
	ThunderDocument* _tmp1_;
	ThunderDocument* _tmp2_;
	ThunderMainWindow* _tmp3_;
#line 33 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	_tmp0_ = thunder_notebook;
#line 33 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	_tmp1_ = thunder_edit_notebook_get_current (_tmp0_);
#line 33 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	_tmp2_ = _tmp1_;
#line 33 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	thunder_document_save (_tmp2_);
#line 34 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	_tmp3_ = thunder_window;
#line 34 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	gtk_widget_show_all ((GtkWidget*) _tmp3_);
#line 198 "HeaderBar.c"
}


static void ___lambda12__gtk_button_clicked (GtkButton* _sender, gpointer self) {
#line 32 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	__lambda12_ ((ThunderHeaderBar*) self);
#line 205 "HeaderBar.c"
}


void thunder_header_bar_header_bar_check (ThunderHeaderBar* self) {
	GtkButton* new_file_button = NULL;
	GtkButton* _tmp0_;
	GtkButton* open_file_button = NULL;
	GtkButton* _tmp1_;
	GtkButton* save_as_file_button = NULL;
	GtkButton* _tmp2_;
	GtkButton* save_file_button = NULL;
	GtkButton* _tmp3_;
#line 9 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	g_return_if_fail (self != NULL);
#line 10 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	_tmp0_ = (GtkButton*) gtk_button_new_from_icon_name ("document-new", GTK_ICON_SIZE_BUTTON);
#line 10 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	g_object_ref_sink (_tmp0_);
#line 10 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	new_file_button = _tmp0_;
#line 11 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	g_signal_connect_object (new_file_button, "clicked", (GCallback) ___lambda4__gtk_button_clicked, self, 0);
#line 15 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	gtk_header_bar_pack_start ((GtkHeaderBar*) self, (GtkWidget*) new_file_button);
#line 17 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	_tmp1_ = (GtkButton*) gtk_button_new_from_icon_name ("document-open", GTK_ICON_SIZE_BUTTON);
#line 17 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	g_object_ref_sink (_tmp1_);
#line 17 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	open_file_button = _tmp1_;
#line 18 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	g_signal_connect_object (open_file_button, "clicked", (GCallback) ___lambda10__gtk_button_clicked, self, 0);
#line 22 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	gtk_header_bar_pack_start ((GtkHeaderBar*) self, (GtkWidget*) open_file_button);
#line 24 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	_tmp2_ = (GtkButton*) gtk_button_new_from_icon_name ("document-save-as", GTK_ICON_SIZE_BUTTON);
#line 24 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	g_object_ref_sink (_tmp2_);
#line 24 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	save_as_file_button = _tmp2_;
#line 25 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	g_signal_connect_object (save_as_file_button, "clicked", (GCallback) ___lambda11__gtk_button_clicked, self, 0);
#line 29 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	gtk_header_bar_pack_end ((GtkHeaderBar*) self, (GtkWidget*) save_as_file_button);
#line 31 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	_tmp3_ = (GtkButton*) gtk_button_new_from_icon_name ("document-save", GTK_ICON_SIZE_BUTTON);
#line 31 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	g_object_ref_sink (_tmp3_);
#line 31 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	save_file_button = _tmp3_;
#line 32 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	g_signal_connect_object (save_file_button, "clicked", (GCallback) ___lambda12__gtk_button_clicked, self, 0);
#line 36 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	gtk_header_bar_pack_end ((GtkHeaderBar*) self, (GtkWidget*) save_file_button);
#line 9 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	_g_object_unref0 (save_file_button);
#line 9 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	_g_object_unref0 (save_as_file_button);
#line 9 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	_g_object_unref0 (open_file_button);
#line 9 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	_g_object_unref0 (new_file_button);
#line 268 "HeaderBar.c"
}


static void thunder_header_bar_class_init (ThunderHeaderBarClass * klass) {
#line 2 "/home/user/Projects/thunderide/src/Widgets/HeaderBar.vala"
	thunder_header_bar_parent_class = g_type_class_peek_parent (klass);
#line 275 "HeaderBar.c"
}


static void thunder_header_bar_instance_init (ThunderHeaderBar * self) {
}


GType thunder_header_bar_get_type (void) {
	static volatile gsize thunder_header_bar_type_id__volatile = 0;
	if (g_once_init_enter (&thunder_header_bar_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (ThunderHeaderBarClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) thunder_header_bar_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (ThunderHeaderBar), 0, (GInstanceInitFunc) thunder_header_bar_instance_init, NULL };
		GType thunder_header_bar_type_id;
		thunder_header_bar_type_id = g_type_register_static (gtk_header_bar_get_type (), "ThunderHeaderBar", &g_define_type_info, 0);
		g_once_init_leave (&thunder_header_bar_type_id__volatile, thunder_header_bar_type_id);
	}
	return thunder_header_bar_type_id__volatile;
}



