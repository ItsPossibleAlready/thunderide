/* MainWindow.c generated by valac 0.36.15, the Vala compiler
 * generated from MainWindow.vala, do not modify */


#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <gio/gio.h>


#define THUNDER_TYPE_HEADER_BAR (thunder_header_bar_get_type ())
#define THUNDER_HEADER_BAR(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), THUNDER_TYPE_HEADER_BAR, ThunderHeaderBar))
#define THUNDER_HEADER_BAR_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), THUNDER_TYPE_HEADER_BAR, ThunderHeaderBarClass))
#define THUNDER_IS_HEADER_BAR(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), THUNDER_TYPE_HEADER_BAR))
#define THUNDER_IS_HEADER_BAR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), THUNDER_TYPE_HEADER_BAR))
#define THUNDER_HEADER_BAR_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), THUNDER_TYPE_HEADER_BAR, ThunderHeaderBarClass))

typedef struct _ThunderHeaderBar ThunderHeaderBar;
typedef struct _ThunderHeaderBarClass ThunderHeaderBarClass;

#define THUNDER_TYPE_EDIT_NOTEBOOK (thunder_edit_notebook_get_type ())
#define THUNDER_EDIT_NOTEBOOK(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), THUNDER_TYPE_EDIT_NOTEBOOK, ThunderEditNotebook))
#define THUNDER_EDIT_NOTEBOOK_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), THUNDER_TYPE_EDIT_NOTEBOOK, ThunderEditNotebookClass))
#define THUNDER_IS_EDIT_NOTEBOOK(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), THUNDER_TYPE_EDIT_NOTEBOOK))
#define THUNDER_IS_EDIT_NOTEBOOK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), THUNDER_TYPE_EDIT_NOTEBOOK))
#define THUNDER_EDIT_NOTEBOOK_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), THUNDER_TYPE_EDIT_NOTEBOOK, ThunderEditNotebookClass))

typedef struct _ThunderEditNotebook ThunderEditNotebook;
typedef struct _ThunderEditNotebookClass ThunderEditNotebookClass;

#define THUNDER_TYPE_MAIN_WINDOW (thunder_main_window_get_type ())
#define THUNDER_MAIN_WINDOW(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), THUNDER_TYPE_MAIN_WINDOW, ThunderMainWindow))
#define THUNDER_MAIN_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), THUNDER_TYPE_MAIN_WINDOW, ThunderMainWindowClass))
#define THUNDER_IS_MAIN_WINDOW(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), THUNDER_TYPE_MAIN_WINDOW))
#define THUNDER_IS_MAIN_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), THUNDER_TYPE_MAIN_WINDOW))
#define THUNDER_MAIN_WINDOW_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), THUNDER_TYPE_MAIN_WINDOW, ThunderMainWindowClass))

typedef struct _ThunderMainWindow ThunderMainWindow;
typedef struct _ThunderMainWindowClass ThunderMainWindowClass;
typedef struct _ThunderMainWindowPrivate ThunderMainWindowPrivate;

#define THUNDER_TYPE_APPLICATION (thunder_application_get_type ())
#define THUNDER_APPLICATION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), THUNDER_TYPE_APPLICATION, ThunderApplication))
#define THUNDER_APPLICATION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), THUNDER_TYPE_APPLICATION, ThunderApplicationClass))
#define THUNDER_IS_APPLICATION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), THUNDER_TYPE_APPLICATION))
#define THUNDER_IS_APPLICATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), THUNDER_TYPE_APPLICATION))
#define THUNDER_APPLICATION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), THUNDER_TYPE_APPLICATION, ThunderApplicationClass))

typedef struct _ThunderApplication ThunderApplication;
typedef struct _ThunderApplicationClass ThunderApplicationClass;
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))

#define THUNDER_TYPE_DOCUMENT (thunder_document_get_type ())
#define THUNDER_DOCUMENT(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), THUNDER_TYPE_DOCUMENT, ThunderDocument))
#define THUNDER_DOCUMENT_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), THUNDER_TYPE_DOCUMENT, ThunderDocumentClass))
#define THUNDER_IS_DOCUMENT(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), THUNDER_TYPE_DOCUMENT))
#define THUNDER_IS_DOCUMENT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), THUNDER_TYPE_DOCUMENT))
#define THUNDER_DOCUMENT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), THUNDER_TYPE_DOCUMENT, ThunderDocumentClass))

typedef struct _ThunderDocument ThunderDocument;
typedef struct _ThunderDocumentClass ThunderDocumentClass;

struct _ThunderMainWindow {
	GtkWindow parent_instance;
	ThunderMainWindowPrivate * priv;
};

struct _ThunderMainWindowClass {
	GtkWindowClass parent_class;
};


extern ThunderHeaderBar* thunder_header_bar;
ThunderHeaderBar* thunder_header_bar = NULL;
extern ThunderEditNotebook* thunder_notebook;
ThunderEditNotebook* thunder_notebook = NULL;
static gpointer thunder_main_window_parent_class = NULL;
extern ThunderMainWindow* thunder_window;

GType thunder_header_bar_get_type (void) G_GNUC_CONST;
GType thunder_edit_notebook_get_type (void) G_GNUC_CONST;
GType thunder_main_window_get_type (void) G_GNUC_CONST;
enum  {
	THUNDER_MAIN_WINDOW_0_PROPERTY
};
GType thunder_application_get_type (void) G_GNUC_CONST;
ThunderMainWindow* thunder_main_window_new (ThunderApplication* ThunderIDE_app);
ThunderMainWindow* thunder_main_window_construct (GType object_type, ThunderApplication* ThunderIDE_app);
ThunderHeaderBar* thunder_header_bar_new (void);
ThunderHeaderBar* thunder_header_bar_construct (GType object_type);
ThunderEditNotebook* thunder_edit_notebook_new (void);
ThunderEditNotebook* thunder_edit_notebook_construct (GType object_type);
void thunder_OpenDialogChooser (void);
GType thunder_document_get_type (void) G_GNUC_CONST;
ThunderDocument* thunder_document_new_from_file (ThunderEditNotebook* notebook, GFile* gfile);
ThunderDocument* thunder_document_construct_from_file (GType object_type, ThunderEditNotebook* notebook, GFile* gfile);


ThunderMainWindow* thunder_main_window_construct (GType object_type, ThunderApplication* ThunderIDE_app) {
	ThunderMainWindow * self = NULL;
	ThunderApplication* _tmp0_;
	ThunderHeaderBar* _tmp1_;
	ThunderEditNotebook* _tmp2_;
	ThunderHeaderBar* _tmp3_;
	ThunderEditNotebook* _tmp4_;
#line 6 "/home/user/Projects/thunderide/src/MainWindow.vala"
	g_return_val_if_fail (ThunderIDE_app != NULL, NULL);
#line 7 "/home/user/Projects/thunderide/src/MainWindow.vala"
	_tmp0_ = ThunderIDE_app;
#line 7 "/home/user/Projects/thunderide/src/MainWindow.vala"
	self = (ThunderMainWindow*) g_object_new (object_type, "application", _tmp0_, NULL);
#line 10 "/home/user/Projects/thunderide/src/MainWindow.vala"
	_tmp1_ = thunder_header_bar_new ();
#line 10 "/home/user/Projects/thunderide/src/MainWindow.vala"
	g_object_ref_sink (_tmp1_);
#line 10 "/home/user/Projects/thunderide/src/MainWindow.vala"
	_g_object_unref0 (thunder_header_bar);
#line 10 "/home/user/Projects/thunderide/src/MainWindow.vala"
	thunder_header_bar = _tmp1_;
#line 11 "/home/user/Projects/thunderide/src/MainWindow.vala"
	_tmp2_ = thunder_edit_notebook_new ();
#line 11 "/home/user/Projects/thunderide/src/MainWindow.vala"
	g_object_ref_sink (_tmp2_);
#line 11 "/home/user/Projects/thunderide/src/MainWindow.vala"
	_g_object_unref0 (thunder_notebook);
#line 11 "/home/user/Projects/thunderide/src/MainWindow.vala"
	thunder_notebook = _tmp2_;
#line 12 "/home/user/Projects/thunderide/src/MainWindow.vala"
	g_object_set ((GtkWindow*) self, "default-height", 450, NULL);
#line 13 "/home/user/Projects/thunderide/src/MainWindow.vala"
	g_object_set ((GtkWindow*) self, "default-width", 800, NULL);
#line 14 "/home/user/Projects/thunderide/src/MainWindow.vala"
	_tmp3_ = thunder_header_bar;
#line 14 "/home/user/Projects/thunderide/src/MainWindow.vala"
	gtk_window_set_titlebar ((GtkWindow*) self, (GtkWidget*) _tmp3_);
#line 15 "/home/user/Projects/thunderide/src/MainWindow.vala"
	_tmp4_ = thunder_notebook;
#line 15 "/home/user/Projects/thunderide/src/MainWindow.vala"
	gtk_container_add ((GtkContainer*) self, (GtkWidget*) _tmp4_);
#line 6 "/home/user/Projects/thunderide/src/MainWindow.vala"
	return self;
#line 143 "MainWindow.c"
}


ThunderMainWindow* thunder_main_window_new (ThunderApplication* ThunderIDE_app) {
#line 6 "/home/user/Projects/thunderide/src/MainWindow.vala"
	return thunder_main_window_construct (THUNDER_TYPE_MAIN_WINDOW, ThunderIDE_app);
#line 150 "MainWindow.c"
}


static void thunder_main_window_class_init (ThunderMainWindowClass * klass) {
#line 5 "/home/user/Projects/thunderide/src/MainWindow.vala"
	thunder_main_window_parent_class = g_type_class_peek_parent (klass);
#line 157 "MainWindow.c"
}


static void thunder_main_window_instance_init (ThunderMainWindow * self) {
}


GType thunder_main_window_get_type (void) {
	static volatile gsize thunder_main_window_type_id__volatile = 0;
	if (g_once_init_enter (&thunder_main_window_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (ThunderMainWindowClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) thunder_main_window_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (ThunderMainWindow), 0, (GInstanceInitFunc) thunder_main_window_instance_init, NULL };
		GType thunder_main_window_type_id;
		thunder_main_window_type_id = g_type_register_static (gtk_window_get_type (), "ThunderMainWindow", &g_define_type_info, 0);
		g_once_init_leave (&thunder_main_window_type_id__volatile, thunder_main_window_type_id);
	}
	return thunder_main_window_type_id__volatile;
}


void thunder_OpenDialogChooser (void) {
	GtkFileChooserDialog* pick = NULL;
	ThunderMainWindow* _tmp0_;
	GtkFileChooserDialog* _tmp1_;
	GtkFileChooserDialog* _tmp2_;
	GtkFileChooserDialog* _tmp3_;
	gint _tmp4_;
	GtkFileChooserDialog* _tmp11_;
#line 19 "/home/user/Projects/thunderide/src/MainWindow.vala"
	_tmp0_ = thunder_window;
#line 19 "/home/user/Projects/thunderide/src/MainWindow.vala"
	_tmp1_ = (GtkFileChooserDialog*) gtk_file_chooser_dialog_new ("Open", (GtkWindow*) _tmp0_, GTK_FILE_CHOOSER_ACTION_OPEN, "Cancel", GTK_RESPONSE_CANCEL, "Open", GTK_RESPONSE_ACCEPT, NULL);
#line 19 "/home/user/Projects/thunderide/src/MainWindow.vala"
	g_object_ref_sink (_tmp1_);
#line 19 "/home/user/Projects/thunderide/src/MainWindow.vala"
	pick = _tmp1_;
#line 26 "/home/user/Projects/thunderide/src/MainWindow.vala"
	_tmp2_ = pick;
#line 26 "/home/user/Projects/thunderide/src/MainWindow.vala"
	gtk_file_chooser_set_select_multiple ((GtkFileChooser*) _tmp2_, FALSE);
#line 27 "/home/user/Projects/thunderide/src/MainWindow.vala"
	_tmp3_ = pick;
#line 27 "/home/user/Projects/thunderide/src/MainWindow.vala"
	_tmp4_ = gtk_dialog_run ((GtkDialog*) _tmp3_);
#line 27 "/home/user/Projects/thunderide/src/MainWindow.vala"
	if (_tmp4_ == ((gint) GTK_RESPONSE_ACCEPT)) {
#line 203 "MainWindow.c"
		ThunderDocument* doc = NULL;
		ThunderEditNotebook* _tmp5_;
		GtkFileChooserDialog* _tmp6_;
		GFile* _tmp7_;
		GFile* _tmp8_;
		ThunderDocument* _tmp9_;
		ThunderDocument* _tmp10_;
#line 28 "/home/user/Projects/thunderide/src/MainWindow.vala"
		_tmp5_ = thunder_notebook;
#line 28 "/home/user/Projects/thunderide/src/MainWindow.vala"
		_tmp6_ = pick;
#line 28 "/home/user/Projects/thunderide/src/MainWindow.vala"
		_tmp7_ = gtk_file_chooser_get_file ((GtkFileChooser*) _tmp6_);
#line 28 "/home/user/Projects/thunderide/src/MainWindow.vala"
		_tmp8_ = _tmp7_;
#line 28 "/home/user/Projects/thunderide/src/MainWindow.vala"
		_tmp9_ = thunder_document_new_from_file (_tmp5_, _tmp8_);
#line 28 "/home/user/Projects/thunderide/src/MainWindow.vala"
		g_object_ref_sink (_tmp9_);
#line 28 "/home/user/Projects/thunderide/src/MainWindow.vala"
		_tmp10_ = _tmp9_;
#line 28 "/home/user/Projects/thunderide/src/MainWindow.vala"
		_g_object_unref0 (_tmp8_);
#line 28 "/home/user/Projects/thunderide/src/MainWindow.vala"
		doc = _tmp10_;
#line 27 "/home/user/Projects/thunderide/src/MainWindow.vala"
		_g_object_unref0 (doc);
#line 231 "MainWindow.c"
	}
#line 30 "/home/user/Projects/thunderide/src/MainWindow.vala"
	_tmp11_ = pick;
#line 30 "/home/user/Projects/thunderide/src/MainWindow.vala"
	gtk_widget_destroy ((GtkWidget*) _tmp11_);
#line 18 "/home/user/Projects/thunderide/src/MainWindow.vala"
	_g_object_unref0 (pick);
#line 239 "MainWindow.c"
}



