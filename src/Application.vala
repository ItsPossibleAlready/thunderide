// modules: gtk+-3.0 MainWindow.vala
/*
* Copyright (c) 2018 FastLine
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA 02110-1301 USA
*
* Authored by: Nurdaulet Kurenshe <nurdauletkurenshe@gmail.com>
*/
namespace Thunder {
    public Thunder.MainWindow window;

    public class Application : Gtk.Application {
        protected override void activate ( ) {
            window = new Thunder.MainWindow (this);
            window.show_all ( );
        }

        public Application ( ) {

        }

        public static int main (string[] args) {
            Application app = new Application ( );
            app.run (args);
            return 0;
        }
    }
}
