namespace Thunder {
    public class HeaderBar : Gtk.HeaderBar {
        public HeaderBar ( ) {
            this.set_title ("ThunderIDE");
            this.set_subtitle ("Write Code Like Thunder");
            this.set_show_close_button (true);
            header_bar_check ( );
        }
        public void header_bar_check ( ) {
            var new_file_button = new Gtk.Button.from_icon_name ("document-new");
                new_file_button.clicked.connect (() => {
                    notebook.new_file ();
                    
                    });
            this.pack_start (new_file_button);

            var open_file_button = new Gtk.Button.from_icon_name ("document-open");
                open_file_button.clicked.connect (() => {
                    Thunder.OpenDialogChooser ();
                    notebook.show_all ();
                    });
            this.pack_start (open_file_button);

            var save_as_file_button = new Gtk.Button.from_icon_name ("document-save-as");
                save_as_file_button.clicked.connect (() => {
                    notebook.current.save_as ();
                    window.show_all ();
                    });
            this.pack_end (save_as_file_button);

            var save_file_button = new Gtk.Button.from_icon_name ("document-save");
                save_file_button.clicked.connect (() => {
                    notebook.current.save ();
                    window.show_all ();
                    });
            this.pack_end (save_file_button);
        }
    }
}
