namespace Thunder {
    public class EditNotebook : Gtk.Notebook {

        public void new_file () {
            var document = new Thunder.Document (this);
        }

        public Document current {
            get {
                return (Document) this.get_nth_page (
                        this.get_current_page()
                    );
            }
        }

        public EditNotebook (){

        }
    }
    public class Document : Gtk.ScrolledWindow {
        public Gtk.SourceBuffer buffer = new Gtk.SourceBuffer (null);
        public Gtk.SourceView text;
        public Thunder.DocumentLabel label;
        public Gtk.SourceFile file;
        public bool untitled = true;

        public Document (EditNotebook notebook) {
            text = new Gtk.SourceView.with_buffer (buffer);
            text.wrap_mode = Gtk.WrapMode.NONE;
            text.indent = 2;
            text.monospace = true;
            text.buffer.text = "";
            text.auto_indent = true;
            text.indent_on_tab = true;
            text.show_line_numbers = true;
            text.highlight_current_line = true;
			text.show_right_margin = true;

            this.add(text);
            label = new Thunder.DocumentLabel("Untitled");
            notebook.append_page (this , label);

            label.close_clicked.connect(() => {
                var pagenum = notebook.page_num (this);
                notebook.remove_page (pagenum);
                });
        }

        public Document.from_file (EditNotebook notebook , File gfile) {
            this (notebook);
            untitled = false;

            file = new Gtk.SourceFile();
            file.location = gfile;
            var source_file_loader = new Gtk.SourceFileLoader(buffer, file);
            label.working = true;
            source_file_loader.load_async.begin (Priority.DEFAULT, null, () => {
			});

            var lm = new Gtk.SourceLanguageManager();
			var language = lm.guess_language(file.location.get_path(), null);
			if (language != null) {
				buffer.language = language;
				buffer.highlight_syntax = true;
			}
            else {
                buffer.highlight_syntax = false;
            }
            label.working = false;
        }
        public void save () {
            if (untitled) {
                var pick = new Gtk.FileChooserDialog ("Save As",
                                                  window ,
                                                  Gtk.FileChooserAction.SAVE,
                                                  "Cancel",
                                                  Gtk.ResponseType.CANCEL,
                                                  "Save",
                                                  Gtk.ResponseType.ACCEPT);
                pick.select_multiple = false;
                if (pick.run() == Gtk.ResponseType.ACCEPT) {
                    file = new Gtk.SourceFile();
                    file.location = pick.get_file ();
                    var lm = new Gtk.SourceLanguageManager();
			        var language = lm.guess_language(file.location.get_path(), null);

			        if (language != null) {
			        	buffer.language = language;
			        	buffer.highlight_syntax = true;
                        buffer.highlight_matching_brackets = true;
                        
			        }
                    else {
                        buffer.highlight_syntax = false;
                    }

                    untitled = false;
                }
                else {
                    pick.destroy ();
                    return;
                }
                pick.destroy ();
            }
            label.working = true;
            var source_file_save = new Gtk.SourceFileSaver(buffer, file);
            source_file_save.save_async.begin (Priority.DEFAULT, null, () => {
                    label.working = false;
                });
        }
        public void save_as () {
            var pick = new Gtk.FileChooserDialog ("Save As",
                                                  window ,
                                                  Gtk.FileChooserAction.SAVE,
                                                  "Cancel",
                                                  Gtk.ResponseType.CANCEL,
                                                  "Save",
                                                  Gtk.ResponseType.ACCEPT);
            pick.select_multiple = false;

            if (pick.run() == Gtk.ResponseType.ACCEPT) {
                file = new Gtk.SourceFile();
                file.location = pick.get_file ();
                var lm = new Gtk.SourceLanguageManager();
			    var language = lm.guess_language(file.location.get_path(), null);
                if (language != null) {
				    buffer.language = language;
			        buffer.highlight_syntax = true;
			    }
                else {
                    buffer.highlight_syntax = false;
                }

                untitled = false;
                pick.destroy ();
            }
            else {
                pick.destroy ();
                return;
            }

            label.working = true;
            var source_file_save = new Gtk.SourceFileSaver(buffer, file);

            source_file_save.save_async.begin (Priority.DEFAULT, null, () => {
                    label.working = false;
            });
        }
    }
    public class DocumentLabel : Gtk.Box {
        private Gtk.Spinner spinner = new Gtk.Spinner();
        private Gtk.Label label;

        public signal void close_clicked();

        public bool working {
            get { return spinner.active; }
            set { spinner.active = value;}
        }

        public DocumentLabel (string label_text) {
            orientation = Gtk.Orientation.HORIZONTAL;
            spacing = 5;
            label = new Gtk.Label (label_text);
            label.expand = true;
            label.margin_start = 45;

            pack_start(label, true, true, 0);

            var button = new Gtk.Button();
            button.relief = Gtk.ReliefStyle.NONE;
            button.add(new Gtk.Image.from_icon_name("window-close", Gtk.IconSize.MENU));
            button.clicked.connect(() => {
                close_clicked();
                });
            pack_end(button, false, false, 0);
            pack_end(spinner, false, false, 0);
            show_all();
        }
    }
}
