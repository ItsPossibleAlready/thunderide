namespace Thunder {
    public class AboutWindow : Gtk.AboutDialog {
        public AboutWindow () {
            this.set_destroy_with_parent (true);

            this.artists = {"Nurdaulet Kurenshe"};
            this.authors = {"Nurdaulet Kurenshe"};

            this.program_name = "ThunderIDE";
            this.comments = "ThunderIDE is a GTK based IDE for Vala programming";
            this.version = "Beta1";
            
            this.response.connect ((response_id) => {
                if (response_id == Gtk.ResponseType.CANCEL || response_id == Gtk.ResponseType.DELETE_EVENT) {
                    this.hide_on_delete ();
                }
            });
            this.present ( );
        }
    }
}
