namespace Thunder {
    public Thunder.HeaderBar header_bar;
    public Thunder.EditNotebook notebook;

    public class MainWindow : Gtk.Window {
        public MainWindow ( Application ThunderIDE_app ){
            Object (
                application: ThunderIDE_app
                );
            header_bar = new Thunder.HeaderBar ();
            notebook = new Thunder.EditNotebook ();
            this.default_height = 450;
            this.default_width = 800;
            this.set_titlebar (header_bar);
            this.add (notebook);
        }
    }
    public void OpenDialogChooser (){
        var pick = new Gtk.FileChooserDialog("Open",
                                            window,
                                            Gtk.FileChooserAction.OPEN,
                                            "Cancel",
                                            Gtk.ResponseType.CANCEL,
                                            "Open",
                                            Gtk.ResponseType.ACCEPT);
        pick.select_multiple = false;
        if (pick.run () == Gtk.ResponseType.ACCEPT) {
            var doc = new Thunder.Document.from_file(notebook , pick.get_file());
        }
        pick.destroy ();
    }
}
